function getThing() {
  return Promise.try(() => {
    return bhttp.get("http://example.com/foobar");
  }).then((response) => {
    return Promise.try(() => {
      return getStatusMessageAsync(response.statusCode);
    }).then((statusMessage) => {
      return `${statusMessage}: ${response.body.toString()}`;
    });
  });
}